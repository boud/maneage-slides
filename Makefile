# Automatically build the slides on the reproducible paper
#
# Copyright (C) 2018-2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This Makefile is part of Maneage. Maneage is free software: you can
# redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either
# version 3 of the License, or (at your option) any later version.
#
# Maneage is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
# more details. See <http://www.gnu.org/licenses/>.


.ONESHELL:


# Clean all extra files
all: final
.PHONY: clean-latex clean all final
clean-latex:
	rm -fv *.aux *.log *.nav *.out *.snm *.toc git-commit.tex
clean: clean-latex
	rm -v *.pdf


images = img/versions_lensing.png


# PDF slides:
# 2024-03-07 slides-intro-short.pdf still tries to load copyright-violating images of non-free research articles
#slides = slides-intro-short.pdf slides-intro.pdf
slides = slides-intro.pdf
$(slides): %.pdf: %.tex tex/*.tex $(images)
        # We'll run pdflatex two times so that the page numbers and
        # internal links are correct.
	if [ -d .git ]; then v=$$(git describe --dirty --always --long); \
	  else                 v=NO-GIT; fi; \
	printf "\\\newcommand{\\gitcommit}{$$v}\n" > git-commit.tex
	pdflatex $*

Roukema20220916TYG.pdf: $(slides)
	N_PAGES=$$(pdfinfo slides-intro.pdf |grep Pages | awk '{print $$2}')
#	pdftk slides-intro.pdf cat 1-$${N_PAGES} output Roukema20220916TYG.pdf # currently superfluous
	pdftk slides-intro.pdf cat 1-3 5-31 34 32-33 4 35 37-94 95 96 97 output Roukema20220916TYG.pdf

Roukema20240311IANCU.pdf: $(slides)
	N_PAGES=$$(pdfinfo slides-intro.pdf |grep Pages | awk '{print $$2}')
#	pdftk slides-intro.pdf cat 1-$${N_PAGES} output Roukema20220916TYG.pdf # currently superfluous
	pdftk slides-intro.pdf cat 1-3 5-30 34 32-33 4 35 37-43 46-59 61-94 95 96 97 output $@

# Ultimate aim of project.
#final: $(slides) Roukema20220916TYG.pdf
final: $(slides) Roukema20240311IANCU.pdf

