Slides to introduce Maneage (managing data lineage)
===================================================

Copyright (C) 2018-2020 Mohammad Akhlaghi <mohammad@akhlaghi.org>\
Copyright (C) 2022 Boud Roukema\
See the end of the file for license conditions.

This is the LaTeX source of slides presenting
[Maneage](https://maneage.org).

To build the slides, you need to have the Beamer and Tikz LaTeX
packages installed. Run this command:

```(shell)
$ make
```

This instruction will use `Makefile` to create a `git-commit.tex` file and then
run LaTeX. So if you want to run LaTeX directly, make this file
manually, see the `Makefile` for the suggested command to generate it.





Copyright information
---------------------

This file is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This file is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this file.  If not, see <https://www.gnu.org/licenses/>.

TODO: Some of the files in this repository still need their licensing
tidied up, especially the image files; the aim is to only include
GPL-3+ compatible files. See
https://gitlab.com/maneage/slides-introduction/-/issues/1 and/or
create an issue at https://codeberg.org/boud/maneage-slides/issues
and/or post a merge request to help with tidying. Thanks!
